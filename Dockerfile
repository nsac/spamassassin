FROM alpine:latest

LABEL maintainer="Richard Brinkman <server@nsac.alpenclub.nl>"

EXPOSE 783

RUN apk --no-cache add spamassassin \
 && mkdir -p /var/spool/mail/.spamassassin \
 && chown -R mail:mail /var/spool/mail \
 && echo loadplugin Mail::SpamAssassin::Plugin::TextCat >> /etc/mail/spamassassin/local.cf \
 && echo ok_languages en de nl fr >> /etc/mail/spamassassin/local.cf \
 && echo ok_locales en >> /etc/mail/spamassassin/local.cf \
 && echo body MODERATION_SEHR_GEEHRTE /Sehr geehrte Damen und Herren/i >> /etc/mail/spamassassin/local.cf \
 && echo score MODERATION_SEHR_GEEHRTE 1 >> /etc/mail/spamassassin/local.cf \
 && echo 'add_header all Level _STARS(*)_' >> /etc/mail/spamassassin/local.cf

VOLUME /var/spool/mail/.spamassassin

COPY entrypoint.sh /usr/local/bin/entrypoint.sh

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

CMD ["spamd", "--syslog=stderr", "--listen=0.0.0.0:783", "--allowed-ips=172.0.0.0/8", "--allow-tell"]
